<html>
  <head>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
      
    <div id="basics">
        <p>Tsolmondorj Nakamoto</p>
  		<p>mongolraider@protonmail.com</p>
		<p>@Mongolraider47 Telegram</p>
    </div>

    <h2>About</h2>
    <div id="about">
          <p>I&#39;ve been working for and with startups for the last few years, building decentralized applications, blockchains, fiancial algorithims , and lots of stuff in between.</p>
          <p>I&#39;m looking for a friendly place to work with competent people I can learn from and teach, healthy life&#x2F;work balance and technical and business challenges. I&#39;m happiest building things that empower people to do more than they could before, and I&#39;m an expert in identifying and seizing opportunities to do so.</p>
    </div>

    <h2>Experience (positions)</h2>

    <div id="positions">
        
        
        <div class="position">
          <p class="title">Cultu.re &mdash; Software Engineer</p>
          <p class="range">2017-Present</p>
          <p class="description"> Replace the Government http://cultu.re </p>
        </div>
        
        <div class="position">
          <p class="title">Future Scouts &mdash; Computer Science & New Economics Fellow (Business Intelligence)</p>
          <p class="range">2017 - Present</p>
          <p class="description">Teaching kids 21st century skills for the 21st century soucts. www.futurescouts.co </p>
        </div>

          <hr>
        <div class="position">
          <p class="title">Trive &mdash; Sr Software Engineer</p>
          <p class="range">2016</p>
          <p class="description">Killing Fake News.  https://trive.news/</p>
        </div>

          <hr>
        <div class="position">
          <p class="title">Nonpariel Captial &mdash; Community Liason Officer
          <p class="range">2015 - Present</p> 
          <p class="description"> Advisor to the crypto community for hedge fund. Nonpariel Capital : An alternative approach to capital markets. http://nonparielcapital.com/

        </div>

          <hr>
        <div class="position">
          <p class="title">Hirsh Singh for Governor &mdash; Data Scientist 
          <p class="range">2017</p>
          <p class="description"> Twitter Bot http://singhfornewjersey.com/ https://en.wikipedia.org/wiki/New_Jersey_Gubernatorial_election,_2017 Raised $1,021,387 Spent $1,016,191 Cash on Hand $5,196 23,728 Votes 9.73%
		</div>

          <hr>
        <div class="position">
          <p class="title">International Coalition for Human Action &mdash; Creative Director 
          <p class="range">2015</p>
          <p class="description">Startupsocieties.com think tank for a progressive world free of oppression. Gained vast knowledges, in organizations, corporations, non profits, and the geo political situation of the world from a micro/macro view point.</p>
        </div>

          <hr>
        <div class="position">
          <p class="title">Carter Media &mdash; Intern 
          <p class="range">2014</p>
          <p class="description">Specialized in high definition video production and post production for online, broadcast, and corporate applications. www.cartermedia.tv 
   
        </div>

    </div>

    <h2>Experience (projects)</h2>

    <div id="projects">
        <div class="position">
          <p class="title">Project A: NewQuant</p>
          <ul class="accomplishments">d
              <li>Under Production</li>
              <li>Beautiful code</li>
              <li>Robust responsive design + front-end code</li>
          </ul>     
          <p class="tech-used">Technologies used: <em>Solidity,Python, MySQL,React, HTML5, CSS3&#x2F;LESS</em></p>
        </div>

          <hr>
        <div class="position">
          <p class="title">Project B: Contracting</p>
          <ul class="accomplishments">
              <li>Design &amp; implement multi-faceted project search UI and backend</li>
              <li>Design &amp; implement contact management UI and backend</li>
              <li>Realize Zawinski&#39;s Law (Design &amp; implement messaging UI and backend)</li>
              <li>Extract user authentication library for reuse</li>
              <li>One-line build + deploy</li>
              <li> HTTPS and Linux server maintenance</li>
              <li>Frontend build pipeline</li>
          </ul>
          <p class="tech-used">Technologies used: <em>Node.js, MySQL, AWS, Braintree</em></p>
        </div>

          <hr>
        <div class="position">
          <p class="title">Project C: Advertisement Agency</p>
          <ul class="accomplishments">
              <li>Multiple landing page designs</li>
              <li>High-converting landing page copy</li>
              <li>Email marketing campaign integration</li>
          </ul>
          <p class="tech-used">Technologies used: <em>WordPress, PHP, MySQL</em></p>
        </div>

    </div>
  </body>
</html>